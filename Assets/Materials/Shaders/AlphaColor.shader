﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/AlphaColor"
{
	Properties
	{
		_Color ("Color", Color ) = (1.0, 1.0, 1.0, 1.0 )
	}
	Category
	{
		Blend SrcAlpha OneMinusSrcAlpha
		SubShader
		{
			Tags
			{
				"Queue" = "AlphaTest"		//this is used for most objects. Opaque geometry uses this queue.
			}

			pass
			{
				ZTest Less
				Offset 1,1 //For wireframe the be showned correctly (may be changed if messh wire frame is introduced)
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma exclude_renderers ps3 xbox360 flash
				#pragma fragmentoption ARB_precision_hint_fastest

				#include "UnityCG.cginc"

				// -- uniforms --
				uniform fixed4 _Color;


				// -- structs --
				struct appdata
				{
					float4 vertex : POSITION;
				};
			
				struct v2f
				{
					float4 position : SV_POSITION;
				};

				v2f vert(appdata i)
				{
					v2f o;
					o.position = UnityObjectToClipPos(i.vertex);

					return o;
				}

				fixed4 frag(v2f i) : COLOR
				{
					return _Color;
				}
				ENDCG
			}
		}
	} 
}
