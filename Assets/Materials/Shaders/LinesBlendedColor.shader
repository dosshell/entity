﻿Shader "Lines/Colored Blended" {
    SubShader {
        Pass {
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite On Cull Off Fog{ Mode Off }
            Offset -1,-1
            BindChannels{ Bind "vertex", vertex Bind "color", color }
        }
    }
}