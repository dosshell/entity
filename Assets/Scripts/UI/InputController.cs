﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour {
    public bool Jump {get; private set; }
    public float Walk { get; private set; }

    private bool jumpTouch = false;
    private float walkTouch = 0f;

	void Start()
    {
        Jump = false;
        Walk = 0f;
	}

	void Update()
    {
        Jump = jumpTouch || Input.GetButton("Jump");
        Walk = Mathf.Clamp(walkTouch + Input.GetAxis("Horizontal"), -1f, 1f);
	}

    void OnJumpPress(float p)
    {
        if (Mathf.Approximately(p, 1f))
            jumpTouch = true;
        else
            jumpTouch = false;
    }

    void OnWalkPress(float p)
    {
        walkTouch += p;
    }

    public void SetInput(string inputName, bool value)
    {
        switch(inputName)
        {
            case "Jump":
                jumpTouch = value;
                break;

            case "WalkLeft":
                walkTouch += value ? -1f : 1f; //Additive therefor 1/-1 to reset
                break;

            case "WalkRight":
                walkTouch += value ? 1f : -1f;
                break;

            default:
                throw new System.NotImplementedException("Input not implemented: " + inputName);
        }
    }
}
