﻿using UnityEngine;
using System.Collections;

public class BackButton : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Application.loadedLevelName == "lobby" ||
                Application.loadedLevelName == "eternity")
                Application.LoadLevel("menu");
            else
                Application.LoadLevel("lobby");
        }
    }
}
