﻿using UnityEngine;
using System.Collections;

public class TouchReader : MonoBehaviour {
    public string InputName;
    public float Radius = 0.1f; //TODO: Auto adjust
    public Texture TextureUp;
    public Texture TextureDown;

    private InputController inputController;

    private bool isOn = true; //Standalone on touchscreens needs to be supported proprly by automagic!


#if UNITY_STANDALONE || UNITY_WEBPLAYER
    private static bool hasTouchRegistred = false; //So we don't turn it off if it has been auto activated
#endif

    public bool IsDown { get; private set; }
    public bool WasDown { get; private set; }

    void Awake()
    {
        inputController = GameObject.FindWithTag("Player").GetComponent<InputController>(); //No linking due to prefab
    }

    void Start()
    {
        IsDown =
        WasDown = false;

        GetComponent<GUITexture>().texture = TextureUp;
        inputController.SetInput(InputName, false);

        //Turn off UI
       //If any touch has been registred: keep UI on
#if UNITY_STANDALONE || UNITY_WEBPLAYER
        if (!hasTouchRegistred)
            TurnOff();
#endif
    }
    
	void Update()
    {
        WasDown = IsDown;
        IsDown = false;

        foreach (Touch c in Input.touches)
        {
#if UNITY_STANDALONE || UNITY_WEBPLAYER
            NotifyTouch();
#endif
            Vector2 p = new Vector2(c.position.x / (float)Screen.width - transform.position.x, c.position.y / (float)Screen.height - transform.position.y);
            if (p.magnitude < Radius)
                IsDown = true;
        }

        if (!IsDown && WasDown)
        {
            GetComponent<GUITexture>().texture = TextureUp;
            inputController.SetInput(InputName, false);
        }
        else if (IsDown && !WasDown)
        {
            GetComponent<GUITexture>().texture = TextureDown;
            inputController.SetInput(InputName, true);
        }
	}

#if UNITY_STANDALONE || UNITY_WEBPLAYER

    private void NotifyTouch()
    {
        hasTouchRegistred = true;
        TurnOn();
    }
#endif

    public void TurnOff()
    {
        if (isOn)
        {
            isOn = false;
            gameObject.GetComponent<GUITexture>().enabled = false;
        }
    }

    public void TurnOn()
    {
        if (!isOn)
        {
            isOn = true;
            gameObject.GetComponent<GUITexture>().enabled = true;
        }
    }

}
