﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraRotate : MonoBehaviour
{
    //TODO: Optimize local variables as statics? No performance impact so why should I? Really?
    private Quaternion TargetRotation;
    public float speed = 500;

    private static List<CameraRotate> objectsToRotate = new List<CameraRotate>();

    void Awake()
    {
        TargetRotation = gameObject.transform.rotation;
    }

    void Start()
    {
        objectsToRotate.Add(this);
    }

    public static void RotateTo(Vector3 dir)
    {
        foreach (CameraRotate cr in objectsToRotate)
            cr.TargetRotation = Quaternion.LookRotation(Vector3.forward, dir);
    }

    void Update()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, TargetRotation, speed * Time.deltaTime);
    }
}
