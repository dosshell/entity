﻿using UnityEngine;
using System.Collections;

public class AspectScale : MonoBehaviour
{
    public float Aspect = 1.0f;

	void Start()
	{
        float a = Camera.main.aspect;
        Vector3 v = transform.localScale;
        v.Set(v.x, a * v.y / Aspect, v.z);
        transform.localScale = v;
	}
}
