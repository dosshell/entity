﻿using UnityEngine;
using System.Collections.Generic;

public class WireAuto : MonoBehaviour
{
    static Material lineMaterial = null;
    private Vector3[] linePoints = null;
    private Color color = LineMap.LineColor;
    public float planeTolerance = 0.001f;

    static void CreateLineMaterial()
    {
        if (lineMaterial == null)
        {
            lineMaterial = new Material(Shader.Find("Lines/Colored Blended"));
        }
    }

    void Start()
    {
        CreateLineMaterial();

        Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
        linePoints = MeshLines(mesh.vertices, mesh.triangles);
    }

    Vector3[] MeshLines(Vector3[] vertices, int[] triangleIndex)
    {
        List<Vector3> triangles = new List<Vector3>();
        List<Vector3> lines = new List<Vector3>();

        //Skapa trianglar
        foreach (int i in triangleIndex)
            triangles.Add(vertices[i]);


        for (int i = 0; i < triangles.Count; i += 3)
        {
            Vector3[] t1 = new Vector3[] { triangles[i], triangles[i + 1], triangles[i + 2] };
            Vector3?[] proposedLines = new Vector3?[6] { t1[0], t1[1], t1[1], t1[2], t1[2], t1[0] };

            //Remove all neighbour lines if it's in the same plane
            for (int j = 0; j < triangles.Count; j += 3)
            {
                Vector3[] t2 = new Vector3[] { triangles[j], triangles[j + 1], triangles[j + 2] };

                //index order: {t1, t2, t1, t2}
                List<int> ni = NeighbourIndexes(t1, t2);

                //Neighbour line found
                if (ni.Count == 4)
                {
                    //Samma plan?
                    //The other points in the triangles which is not overlapping
                    int mi_t1 = 3 - (ni[0] + ni[2]);
                    int mi_t2 = 3 - (ni[1] + ni[3]);


                    Plane plane1 = new Plane(t1[0], t1[1], t1[2]);

                    if (Mathf.Abs(plane1.GetDistanceToPoint(t2[mi_t2])) < planeTolerance)
                    {
                        if (mi_t1 == 0)
                        {
                            proposedLines[2] = null;
                            proposedLines[3] = null;
                        }
                        else if (mi_t1 == 1)
                        {
                            proposedLines[4] = null;
                            proposedLines[5] = null;
                        }
                        else if (mi_t1 == 2)
                        {
                            proposedLines[0] = null;
                            proposedLines[1] = null;
                        }
                    }
                }
            }

            
            //Add remaining proposed lines to the real lines vector
            for (int p = 0; p < proposedLines.Length; ++p)
                if (proposedLines[p] != null)
                    lines.Add( (Vector3) proposedLines[p] );
        }

        //Remove dublication of lines

        //Finnish! :D
        return lines.ToArray();
    }

    List<int> NeighbourIndexes(Vector3[] a, Vector3[] b)
    {
        List<int> neighbours = new List<int>();
        for (int i = 0; i < 3; ++i)
            for (int j = 0; j < 3; ++j)
                if (a[i] == b[j])
                {
                    neighbours.Add(i);
                    neighbours.Add(j);
                }

        return neighbours;
    }

    void OnRenderObject()
    {
        lineMaterial.SetPass(0);

        GL.PushMatrix();
            GL.MultMatrix(transform.localToWorldMatrix);
            GL.Begin(GL.LINES);

            GL.Color(color);

            //Skriv ut endast linjer
            for (int i = 0; i < linePoints.Length; ++i)
                GL.Vertex(linePoints[i]);

            GL.End();
        GL.PopMatrix();
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (lineMaterial == null || linePoints == null)
            Start();

        OnRenderObject();
    }
#endif
}
