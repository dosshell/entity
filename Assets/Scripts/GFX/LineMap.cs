﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class LineMap : MonoBehaviour
{
    private static Material lineMaterial = null;
    private static GameObject instance = null;
    private static List<Vector3> linePointsX = new List<Vector3>();
    private static List<Vector3> linePointsY = new List<Vector3>();
    private static List<Vector3> linePointsZ = new List<Vector3>();
    private static List<Vector3> linePointsRaw = new List<Vector3>();

    public static readonly Color LineColor = new Color(0f, 0f, 0f, 0.5f);
    public static readonly Color StartEdgeColor = LineColor;
    public static readonly Color EndEdgeColor = new Color(0f, 0f, 0f, 0.0f);
    public static readonly float EdgeLength = 0.15f;

    static void Init()
    {
        if (!IsInstantied())
        {
            instance = new GameObject("LineMap");
            instance.AddComponent<LineMap>();
        }
    }

    static bool IsInstantied()
    {
        return instance != null;
    }

    void Awake()
    {
        CreateLineMaterial();

        linePointsX.Clear();
        linePointsY.Clear();
        linePointsZ.Clear();
        linePointsRaw.Clear();
    }

    static public void AddRawLine(Vector3 p1, Vector3 p2, Transform t = null)
    {
        if (!IsInstantied())
            Init();

        p1 = TranslatePoint(p1, t);
        p2 = TranslatePoint(p2, t);

        linePointsRaw.Add(p1);
        linePointsRaw.Add(p2);
    }

    static private Vector3 TranslatePoint(Vector3 p, Transform t = null)
    {
        if (t != null)
            p = t.TransformPoint(p);

        return new Vector3(
            (float)Math.Round(p.x, 3),
            (float)Math.Round(p.y, 3),
            (float)Math.Round(p.z, 3));
    }

    static public void AddLine(Vector3 p1, Vector3 p2, Transform t = null)
    {
        if (!IsInstantied())
            Init();

        p1 = TranslatePoint(p1, t);
        p2 = TranslatePoint(p2, t);

        if (p1.z == p2.z)
        {
            if (p1.x == p2.x)
            {
                linePointsY.Add(p1);
                linePointsY.Add(p2);
                linePointsY = linePointsY.OrderBy(v => v.z).ThenBy(v => v.x).ThenBy(v => v.y).ToList();
            }
            else if (p1.y == p2.y)
            {
                linePointsX.Add(p1);
                linePointsX.Add(p2);
                linePointsX = linePointsX.OrderBy(v => v.z).ThenBy(v => v.y).ThenBy(v => v.x).ToList();
            }
            else
            {
                throw new Exception("Invalid line!");
            }
        }
        else
        {
            //Toggle Z-lines
            bool hasRemoved = false;
            for (int i = 0; i < linePointsZ.Count; i += 2)
            {
                if (linePointsZ[i] == p1 && linePointsZ[i + 1] == p2)
                {
                    linePointsZ.RemoveAt(i);
                    linePointsZ.RemoveAt(i);

                    hasRemoved = true;
                    break;
                }
            }
            if (!hasRemoved)
            {
                linePointsZ.Add(p1);
                linePointsZ.Add(p2);
            }
        }
    }

    static void CreateLineMaterial()
    {
        if (lineMaterial == null)
        {
            lineMaterial = new Material(Shader.Find("Lines/Colored Blended"));
        }
    }

    void OnRenderObject()
    {
        lineMaterial.SetPass(0);

        GL.PushMatrix();
        GL.Begin(GL.LINES);

            GL.Color(LineColor);
        
            Vector3 dx = new Vector3(EdgeLength, 0f, 0f);
            Vector3 dy = new Vector3(0f, EdgeLength, 0f);

            for (int i = 0; i + 1 < linePointsX.Count; i+=2)
            {
                var p1 = linePointsX[i];
                var p2 = linePointsX[i + 1];

                if (p1 != p2)
                {
                    GL.Color(EndEdgeColor);
                    GL.Vertex(p1 - dx);
                    GL.Color(StartEdgeColor);
                    GL.Vertex(p1);

                    GL.Color(LineColor);
                    GL.Vertex(p1);
                    GL.Vertex(p2);

                    GL.Color(StartEdgeColor);
                    GL.Vertex(p2);
                    GL.Color(EndEdgeColor);
                    GL.Vertex(p2 + dx);
                }
            }

            for (int i = 0; i + 1 < linePointsY.Count; i += 2)
            {
                var p1 = linePointsY[i];
                var p2 = linePointsY[i + 1];

                if (p1 != p2)
                {
                    GL.Color(EndEdgeColor);
                    GL.Vertex(p1 - dy);
                    GL.Color(StartEdgeColor);
                    GL.Vertex(p1);

                    GL.Color(LineColor);
                    GL.Vertex(p1);
                    GL.Vertex(p2);

                    GL.Color(StartEdgeColor);
                    GL.Vertex(p2);
                    GL.Color(EndEdgeColor);
                    GL.Vertex(p2 + dy);
                }
            }

            GL.Color(LineColor);

            foreach (Vector3 v in linePointsZ)
                GL.Vertex(v);
        

            //Vad fan gör det här? Om man manuellt vill rita en linje som inte är vinkelrät?
            //foreach (Vector3 v in linePointsRaw)
            //    GL.Vertex(v);


        GL.End();
        GL.PopMatrix();
    }
}
