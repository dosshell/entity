﻿using UnityEngine;
using System.Collections;

public class FovAnimate : MonoBehaviour
{
    private float StartFov = 120f;
    private Vector3 StartPos = new Vector3(0f, 0f, -1.7f);

    private float AnimTime = 0.6f;
    
    private float EndFov;
    private float FovSpeed;

    private Vector3 EndPos;
    private Vector3 PosSpeed;

    private Camera cam = null;

	void Awake()
	{
        cam = GetComponent<Camera>();
	}

	void Start()
	{
        EndFov = cam.fieldOfView;
        FovSpeed = (StartFov - EndFov) / AnimTime;

        EndPos = cam.transform.position;
        PosSpeed = (StartPos - EndPos) / AnimTime;

        cam.fieldOfView = StartFov;
        cam.transform.position = StartPos;

	}

	void Update()
	{
        
        if (cam.fieldOfView > EndFov)
        {
            cam.fieldOfView = cam.fieldOfView - FovSpeed * Time.deltaTime;
            cam.transform.position = cam.transform.position - PosSpeed * Time.deltaTime;
        }
	}
}
