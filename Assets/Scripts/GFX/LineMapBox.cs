﻿using UnityEngine;
using System.Collections;

public class LineMapBox : MonoBehaviour
{
    static Material lineMaterial = null;
    public Color LineColor = LineMap.LineColor;

    private static Vector3 ftl = new Vector3(-0.5f, +0.5f, -0.5f);
    private static Vector3 ftr = new Vector3(+0.5f, +0.5f, -0.5f);
    private static Vector3 fbl = new Vector3(-0.5f, -0.5f, -0.5f);
    private static Vector3 fbr = new Vector3(+0.5f, -0.5f, -0.5f);

    private static Vector3 btl = new Vector3(-0.5f, +0.5f, +0.5f);
    private static Vector3 btr = new Vector3(+0.5f, +0.5f, +0.5f);
    private static Vector3 bbl = new Vector3(-0.5f, -0.5f, +0.5f);
    private static Vector3 bbr = new Vector3(+0.5f, -0.5f, +0.5f);


    void Start()
    {
        //Front
        LineMap.AddLine(ftl, ftr, transform);
        LineMap.AddLine(fbl, fbr, transform);
        LineMap.AddLine(fbl, ftl, transform);
        LineMap.AddLine(fbr, ftr, transform);

        //Sides
        LineMap.AddLine(ftl, btl, transform);
        LineMap.AddLine(ftr, btr, transform);
        LineMap.AddLine(fbl, bbl, transform);
        LineMap.AddLine(fbr, bbr, transform);

        LineMap.AddLine(btl, btr, transform);
        LineMap.AddLine(bbl, bbr, transform);
        LineMap.AddLine(bbl, btl, transform);
        LineMap.AddLine(bbr, btr, transform);
    }

#if UNITY_EDITOR
    static void CreateLineMaterial()
    {
        if (lineMaterial == null)
        {
            lineMaterial = new Material(Shader.Find("Lines/Colored Blended"));
        }
    }

    void OnDrawGizmos()
    {
        CreateLineMaterial();

        lineMaterial.SetPass(0);

        GL.PushMatrix();
            GL.MultMatrix(transform.localToWorldMatrix);
            GL.Begin(GL.LINES);

                GL.Color(LineColor);
                GL.Vertex(ftl); GL.Vertex(ftr);
                GL.Vertex(fbl); GL.Vertex(fbr);
                GL.Vertex(ftl); GL.Vertex(fbl);
                GL.Vertex(ftr); GL.Vertex(fbr);

                GL.Vertex(btl); GL.Vertex(btr);
                GL.Vertex(bbl); GL.Vertex(bbr);
                GL.Vertex(btl); GL.Vertex(bbl);
                GL.Vertex(btr); GL.Vertex(bbr);

                GL.Vertex(ftl); GL.Vertex(btl);
                GL.Vertex(fbl); GL.Vertex(bbl);
                GL.Vertex(ftr); GL.Vertex(btr);
                GL.Vertex(fbr); GL.Vertex(bbr);

            GL.End();
        GL.PopMatrix();
    }
#endif
}
