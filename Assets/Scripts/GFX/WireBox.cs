﻿using UnityEngine;
using System.Collections;

public class WireBox : MonoBehaviour
{
    public bool DrawEdges = false;
    public Shader LineShader;
    private static Material lineMaterial = null;
    public float EdgeLength = LineMap.EdgeLength;

    private Color LineColor = LineMap.LineColor;
    private Color StartEdgeColor = LineMap.StartEdgeColor;
    private Color EndEdgeColor = LineMap.EndEdgeColor;

    private static Vector3 ftl = new Vector3(-0.5f, +0.5f, -0.5f);
    private static Vector3 ftr = new Vector3(+0.5f, +0.5f, -0.5f);
    private static Vector3 fbl = new Vector3(-0.5f, -0.5f, -0.5f);
    private static Vector3 fbr = new Vector3(+0.5f, -0.5f, -0.5f);

    private static Vector3 btl = new Vector3(-0.5f, +0.5f, +0.5f);
    private static Vector3 btr = new Vector3(+0.5f, +0.5f, +0.5f);
    private static Vector3 bbl = new Vector3(-0.5f, -0.5f, +0.5f);
    private static Vector3 bbr = new Vector3(+0.5f, -0.5f, +0.5f);

    void CreateLineMaterial()
    {
        if (lineMaterial == null)
        {
            lineMaterial = new Material(LineShader);
        }
    }

    void Awake()
    {
        CreateLineMaterial();
    }

    void OnRenderObject()
    {
        CreateLineMaterial();

        lineMaterial.SetPass(0);

        GL.PushMatrix();
            GL.MultMatrix(transform.localToWorldMatrix);
            GL.Begin(GL.LINES);

                GL.Color(LineColor);

                GL.Vertex(ftl); GL.Vertex(ftr);
                GL.Vertex(fbl); GL.Vertex(fbr);
                GL.Vertex(ftl); GL.Vertex(fbl);
                GL.Vertex(ftr); GL.Vertex(fbr);

                GL.Vertex(btl); GL.Vertex(btr);
                GL.Vertex(bbl); GL.Vertex(bbr);
                GL.Vertex(btl); GL.Vertex(bbl);
                GL.Vertex(btr); GL.Vertex(bbr);

                GL.Vertex(ftl); GL.Vertex(btl);
                GL.Vertex(fbl); GL.Vertex(bbl);
                GL.Vertex(ftr); GL.Vertex(btr);
                GL.Vertex(fbr); GL.Vertex(bbr);

                if (DrawEdges)
                {
                    Vector3 dx = new Vector3(EdgeLength / transform.localScale.x, 0f, 0f);
                    Vector3 dy = new Vector3(0f, EdgeLength / transform.localScale.y, 0f);

                    GL.Color(StartEdgeColor); GL.Vertex(ftl); GL.Color(EndEdgeColor); GL.Vertex(ftl - dx);
                    GL.Color(StartEdgeColor); GL.Vertex(btl); GL.Color(EndEdgeColor); GL.Vertex(btl - dx);

                    GL.Color(StartEdgeColor); GL.Vertex(ftl); GL.Color(EndEdgeColor); GL.Vertex(ftl + dy);
                    GL.Color(StartEdgeColor); GL.Vertex(btl); GL.Color(EndEdgeColor); GL.Vertex(btl + dy);

                    GL.Color(StartEdgeColor); GL.Vertex(ftr); GL.Color(EndEdgeColor); GL.Vertex(ftr + dy);
                    GL.Color(StartEdgeColor); GL.Vertex(btr); GL.Color(EndEdgeColor); GL.Vertex(btr + dy);

                    GL.Color(StartEdgeColor); GL.Vertex(ftr); GL.Color(EndEdgeColor); GL.Vertex(ftr + dx);
                    GL.Color(StartEdgeColor); GL.Vertex(btr); GL.Color(EndEdgeColor); GL.Vertex(btr + dx);

                    GL.Color(StartEdgeColor); GL.Vertex(fbr); GL.Color(EndEdgeColor); GL.Vertex(fbr + dx);
                    GL.Color(StartEdgeColor); GL.Vertex(bbr); GL.Color(EndEdgeColor); GL.Vertex(bbr + dx);

                    GL.Color(StartEdgeColor); GL.Vertex(fbr); GL.Color(EndEdgeColor); GL.Vertex(fbr - dy);
                    GL.Color(StartEdgeColor); GL.Vertex(bbr); GL.Color(EndEdgeColor); GL.Vertex(bbr - dy);

                    GL.Color(StartEdgeColor); GL.Vertex(fbl); GL.Color(EndEdgeColor); GL.Vertex(fbl - dy);
                    GL.Color(StartEdgeColor); GL.Vertex(bbl); GL.Color(EndEdgeColor); GL.Vertex(bbl - dy);

                    GL.Color(StartEdgeColor); GL.Vertex(fbl); GL.Color(EndEdgeColor); GL.Vertex(fbl - dx);
                    GL.Color(StartEdgeColor); GL.Vertex(bbl); GL.Color(EndEdgeColor); GL.Vertex(bbl - dx);
                }

            GL.End();
        GL.PopMatrix();
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        OnRenderObject();
    }
#endif
}
