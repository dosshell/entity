﻿using UnityEngine;
using System.Collections;

public class WireFrame : MonoBehaviour
{
    private static Material lineMaterial = null;
    private int[] traingles;
    private Vector3[] vertices;
    private Color color = LineMap.LineColor;

    static void CreateLineMaterial()
    {
        if (lineMaterial == null)
        {
            lineMaterial = new Material(Shader.Find("Lines/Colored Blended"));
        }
    }

    void Start()
    {
        CreateLineMaterial();

        Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
        traingles = mesh.triangles;
        vertices = mesh.vertices;
    }

    void OnRenderObject()
    {
        lineMaterial.SetPass(0);

        GL.PushMatrix();
        GL.MultMatrix(transform.localToWorldMatrix);

        GL.Begin(GL.LINES);
            GL.Color(color);
            for (int i = 0; i < traingles.Length; i += 3)
            {
                GL.Vertex(vertices[traingles[i + 0]]);
                GL.Vertex(vertices[traingles[i + 1]]);
                GL.Vertex(vertices[traingles[i + 1]]);
                GL.Vertex(vertices[traingles[i + 2]]);
                GL.Vertex(vertices[traingles[i + 2]]);
                GL.Vertex(vertices[traingles[i + 0]]);
            }
        GL.End();
        GL.PopMatrix();
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (lineMaterial == null || traingles == null || vertices == null)
            Start();

        OnRenderObject();
    }
#endif
}
