﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Persistent : MonoBehaviour
{
    private static Dictionary<string, GameObject> t = new Dictionary<string, GameObject>();
    public bool Replace = false;

    void Awake()
    {
        string identifier = this.gameObject.name;


        if (t.ContainsKey(identifier))
        {
            if (Replace)
            {
                GameObject old = t[identifier];
                t.Remove(identifier);
                Destroy(old);
                MakePersistent(this.gameObject);
            }
            else
                Destroy(this.gameObject);
        }
        else
            MakePersistent(this.gameObject);
    }

    private void MakePersistent(GameObject obj)
    {
        DontDestroyOnLoad(obj);
        t.Add(obj.name, obj);
    }
}
