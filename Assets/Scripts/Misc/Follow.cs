﻿using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour
{
    private Transform TransformToFollow;
    public bool FollowX = true;
    public bool FollowY = true;
    public bool FollowZ = false;

    public float MaxX = float.PositiveInfinity;
    public float MinX = float.NegativeInfinity;
    public float MaxY = float.PositiveInfinity;
    public float MinY = float.NegativeInfinity;
    public float MaxZ = float.PositiveInfinity;
    public float MinZ = float.NegativeInfinity;

    void Awake()
    {
        TransformToFollow = GameObject.FindGameObjectWithTag("Player").transform;
    }

	void LateUpdate()
    {
        transform.position = new Vector3(
            FollowX ? Mathf.Max(MinX, Mathf.Min(MaxX, TransformToFollow.position.x)) : transform.position.x,
            FollowY ? Mathf.Max(MinY, Mathf.Min(MaxY, TransformToFollow.position.y)) : transform.position.y,
            FollowZ ? Mathf.Max(MinZ, Mathf.Min(MaxZ, TransformToFollow.position.z)) : transform.position.z);
	}
}
