﻿using UnityEngine;
using System.Collections;

public class Playlist : MonoBehaviour
{
    public bool Shuffle = false;
    public bool ShuffleStart = true;

    public AudioClip[] List;

    public AudioClip CurrentTrack { get; set; }
    public int CurrentTrackNumber { get; set; }
    private AudioSource audioSoruce;

    void Awake()
    {
        audioSoruce = GetComponent<AudioSource>();
    }

    void Start()
    {
        if (!audioSoruce.isPlaying)
        {
            if (ShuffleStart)
                CurrentTrackNumber = Random.Range(0, List.Length);

            NextTrack();
        }
    }

    void NextTrack()
    {
       
        if (Shuffle)
            CurrentTrackNumber = Random.Range(0, List.Length);
        else
            CurrentTrackNumber = ++CurrentTrackNumber % List.Length;

        CurrentTrack = List[CurrentTrackNumber];

        audioSoruce.clip = CurrentTrack;
        audioSoruce.Play();
    }

    void Update()
    {
        if (!audioSoruce.isPlaying && CurrentTrack.loadState == AudioDataLoadState.Loaded)
        {
            NextTrack();
        }
    }
}
