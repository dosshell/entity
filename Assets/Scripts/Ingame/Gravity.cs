﻿using UnityEngine;
using System.Collections;

public class Gravity : MonoBehaviour
{
    public static Vector2 Acceleration = -Vector2.up;

    //For easy difinition of direction and magnitude
    public static Vector2 Direction
    {
        get { return Acceleration.normalized; }
        set { Acceleration = value.normalized * Acceleration.magnitude; }
    }
    public static float Magnitude
    {
        get { return Acceleration.magnitude; }
        set { Acceleration = Acceleration.normalized * value; }
    }

    public bool UseGravity = true;
    public bool UseConstantDirection = false;
    //For unity editor
    public Vector2 ConstantDirection = -Vector2.up; //Still using magnitude from global

    public bool UseDrag = true; //For easy restore
    public float DragCoefficient = 10f;

	void Awake()
	{
        Direction = Physics2D.gravity.normalized; //From PhysicsManager. Only for init (right now)!!!
        Magnitude = Physics2D.gravity.magnitude;
	}

	void FixedUpdate()
	{
        if (GetComponent<Rigidbody2D>() != null)
        {
            if (UseGravity)
                GetComponent<Rigidbody2D>().AddForce((UseConstantDirection ? ConstantDirection.normalized : Direction) * Magnitude, ForceMode2D.Force);

            if (UseDrag)
                GetComponent<Rigidbody2D>().AddForce(-GetComponent<Rigidbody2D>().velocity * DragCoefficient, ForceMode2D.Force);
        }
	}
}
