﻿using UnityEngine;
using System.Collections;

public class Kill : MonoBehaviour {
    private bool isActive = false;

    void Start()
    {
        isActive = true;
    }
    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag == "Player" && isActive)
            Application.LoadLevel(Application.loadedLevelName);
    }
}
