﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Movement : MonoBehaviour
{
    public bool IsGrounded { get; private set; }

    public LayerMask Ground;
    public float WalkSpeed  = 40f;

    public float JumpImpulse    = 8f;
    public float JumpPushForce  = 180f;
    public float PushTime       = 0.11f;

    private float pushTimeLeft  = 0f;

    private InputController inputController = null;
    private List<Key> keys = new List<Key>();
    public bool WasGrounded { get; private set; }

    void Start()
    {
        IsGrounded =
        WasGrounded = false;

        inputController = gameObject.GetComponent<InputController>();
        foreach (GameObject k in GameObject.FindGameObjectsWithTag("Key"))
            keys.Add(k.GetComponent<Key>());
    }

    void FixedUpdate()
    {
        // -- GROUNDING --
        WasGrounded = IsGrounded;
        RaycastHit2D rh = Physics2D.CircleCast(transform.position, 0.24f, Gravity.Direction, 0.025f, Ground);
        IsGrounded = rh.collider != null;

        //Reset stuff when landing (bugfix for doublepress in air)
        if (IsGrounded && !WasGrounded)
            foreach (Key k in keys)
                k.SetDefault();



        // -- WALK --
        Vector2 walkDirection = Camera.main.transform.localToWorldMatrix * Vector2.right;
        GetComponent<Rigidbody2D>().AddForce(inputController.Walk * WalkSpeed * walkDirection, ForceMode2D.Force);

        // -- JUMP --
        if (!IsGrounded && IsAltitudeStable()) //No howering under roof
            pushTimeLeft = 0f;

        if (inputController.Jump)
        {
            if (WasGrounded && IsGrounded) //Bugfix: Bounce-error
            {
                GetComponent<Rigidbody2D>().AddForce(-Gravity.Direction * JumpImpulse, ForceMode2D.Impulse);
                pushTimeLeft = PushTime;
            }
            else if (pushTimeLeft > 0f)
                pushTimeLeft -= Time.fixedDeltaTime;
        }
        else
            pushTimeLeft = 0f; //No resuming

        //Apply jump-pushes
        if (pushTimeLeft > 0f)
        {
            GetComponent<Rigidbody2D>().AddForce(-Gravity.Acceleration, ForceMode2D.Force); //Or turn off? Doesn't matter
            GetComponent<Rigidbody2D>().AddForce(-Gravity.Direction * JumpPushForce, ForceMode2D.Force);
        }
    }

    private bool IsAltitudeStable()
    {
        //We get the velocity in the gravity direction
        return Mathf.Approximately(Vector2.Dot(GetComponent<Rigidbody2D>().velocity, Gravity.Direction), 0f);
    }
}
