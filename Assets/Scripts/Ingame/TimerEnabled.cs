﻿using UnityEngine;
using System.Collections;

public class TimerEnabled : MonoBehaviour
{
    public float StartedStateTime = 1f;
    public float ChangedStateTime = 1f;
    public float TimeOffset = 0f;

    private Switch switcher;
    private bool state;

    void Start()
    {
        switcher = gameObject.GetComponent<Switch>();
        state = switcher.IsOn;
    }

	void Update()
	{
        if (state != ComputedState())
        {
            switcher.Toggle();
            state = !state;
        }
	}

    private bool ComputedState()
    {
        float dt = (Time.timeSinceLevelLoad + TimeOffset) % (StartedStateTime + ChangedStateTime);
        return (dt <= StartedStateTime);
    }
}
