﻿using UnityEngine;
using System.Collections;

public class LevelUnlocker : MonoBehaviour
{
    public bool IsUnlocked(int i)
    {
        string keyName = "game" + i;
        return PlayerPrefs.HasKey(keyName) && PlayerPrefs.GetInt(keyName) > 0;
    }

    public void Unlock(int i)
    {
        PlayerPrefs.SetInt("game" + i, 1);
        PlayerPrefs.Save();
        GetComponent<AudioSource>().Play();
    }

    public void LockAll()
    {
        PlayerPrefs.DeleteAll();
    }
}
