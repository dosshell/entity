﻿using UnityEngine;
using System.Collections;

public abstract class Switch : MonoBehaviour
{
    public bool IsStartedOn = true;
    public bool IsOn { get; private set; }

    protected void Awake()
    {
        IsOn = IsStartedOn;
    }

    void Start()
    {
        if (!IsStartedOn)
        {
            IsOn = !IsOn; //Ugly fix... but it works :) (Race conditions fix)
            Toggle();
        }
    }

    public void Toggle()
    {
        SetSwitch(!IsOn);
    }

    public void SetSwitch(bool open)
    {
        IsOn = open;
        OnSwitch(open);
    }

    protected abstract void OnSwitch(bool b);
}
