﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Exit : MonoBehaviour
{
    public string Level;

    public int UnlockDoor = -1;
    LevelUnlocker unlocker = null;
    void Awake()
    {
        GameObject go = GameObject.FindWithTag("LevelUnlocker");
        if (go != null)
            unlocker = go.GetComponent<LevelUnlocker>();
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag == "Player")
        {
            if (UnlockDoor >= 0 && unlocker != null)
                unlocker.Unlock(UnlockDoor);

            ChangeLevel();
        }
    }

    void ChangeLevel()
    {
        SceneManager.LoadScene(Level);
    }
}
