﻿using UnityEngine;
using System.Collections.Generic;

public class Key : MonoBehaviour
{
    public GameObject[] Switches;
    public AudioClip OnSound;
    public AudioClip OffSound;
    public bool IsStartedAsOn = false; //For sounds
    public bool IsAutoReset = false;
    public bool IsOn { get; private set; }

    public Material OnMaterial;
    public Material OffMaterial;

    private bool IsAlreadyTouched = false;
    private List<Switch> switchList = new List<Switch>();
    private int numberOfContacts = 0;
    private Movement mv;

    void Awake()
    {
        foreach (GameObject gameobjectet in Switches)
        {
            if (gameobjectet != null)
            {
                Switch switchen = gameobjectet.GetComponent<Switch>();
                if (switchen != null)
                    switchList.Add(switchen);
                else
                    throw new System.InvalidCastException(name + ": No switch detected in linked GameObject");
            }
            else
                throw new System.InvalidCastException(name + ": Key has empty linked switch");
        }
    }

    void Start()
    {
        IsOn = IsStartedAsOn;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        ++numberOfContacts;
        if (numberOfContacts == 1)
        {
            if (!IsAlreadyTouched)
            {
                ToggleSwitches();

                if (other.tag == "Player")
                    if (!other.gameObject.GetComponent<Movement>().IsGrounded)
                        IsAlreadyTouched = true;
            }
        }
    }

    public void SetDefault()
    {
        IsAlreadyTouched = false;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        --numberOfContacts;
        if (numberOfContacts == 0)
        {
            if (IsAutoReset)
            {
                SetDefault();
                ToggleSwitches();
            }
        }
    }

    private void ToggleSwitches()
    {
        IsOn = !IsOn;
        GetComponent<AudioSource>().PlayOneShot(IsOn ? OnSound : OffSound);

        foreach (Switch sw in switchList)
            sw.Toggle();


        //Stop/Start Animation
        var l = GetComponentsInChildren<Rotate>();
        foreach (var k in l)
            k.IsRotating = !IsOn;

        var o = GetComponentsInChildren<MeshRenderer>();
        foreach (var i in o)
            i.material = IsOn ? OnMaterial : OffMaterial;
    }
}
