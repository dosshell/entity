﻿using UnityEngine;
using System.Collections;

public class GravityChanger : MonoBehaviour
{
    public Vector3[] GravityDirections;
    private int counter = 0;
    private Quaternion RotationTarget;
    private float RotationSpeed = 800f;

    float minTimeBetween = 0.2f;
    float lastActivation = float.NegativeInfinity;

    void Start()
    {
        this.gameObject.transform.rotation = Quaternion.LookRotation(Vector3.forward, GravityDirections[counter]);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (GravityDirections.Length > 0)
        {
            //Quick repeat lockfix
            if (Time.time > lastActivation + minTimeBetween)
            {
                GetComponent<AudioSource>().Play();
                lastActivation = Time.time;

                Vector3 dir = GravityDirections[counter];
                other.transform.rotation = Quaternion.LookRotation(Vector3.forward, -dir);
                Gravity.Direction = dir;

                //Change camera 90 degrees and rotate some stuff
                if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y))
                    CameraRotate.RotateTo(Vector3.left);
                else
                    CameraRotate.RotateTo(Vector3.up);

                counter = ++counter % GravityDirections.Length;
                Vector3 newDir = GravityDirections[counter];
                RotationTarget = Quaternion.LookRotation(Vector3.forward, newDir);
            }
        }
    }

    void Update()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, RotationTarget, RotationSpeed * Time.deltaTime);
    }
}
