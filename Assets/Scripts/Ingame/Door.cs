﻿using UnityEngine;
using System.Collections;

public class Door : Switch
{
    private MeshRenderer mr = null;
    private Collider2D c = null;
    
    new void Awake()
    {
        base.Awake();

        mr = gameObject.GetComponent<MeshRenderer>();
        c = gameObject.GetComponent<Collider2D>();
        if (mr == null || c == null)
            throw new System.InvalidCastException("You need a mesh and a collider on the Component");
    }

    protected override void OnSwitch(bool b)
    {
        mr.enabled  = b;
        c.enabled   = b;
    }
}
