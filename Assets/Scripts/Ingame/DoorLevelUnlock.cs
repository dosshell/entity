﻿using UnityEngine;
using System.Collections;

public class DoorLevelUnlock : MonoBehaviour {
    public int UnlockId = -1;
    private LevelUnlocker _unlocker;
    private Switch _sw;

    void Awake()
    {
        GameObject go = GameObject.FindGameObjectWithTag("LevelUnlocker");
        if (go != null)
            _unlocker = go.GetComponent<LevelUnlocker>();
        else
            Debug.Log("No LevelUnlocker-tag found");

        _sw = GetComponent<Switch>();
    }
    void Start()
    {
    if (UnlockId >= 0)
        {
            if (_unlocker != null && _sw != null)
            {
                if (_unlocker.IsUnlocked(UnlockId))
                    _sw.Toggle();
            }
            else
                Debug.Log("Unlocker component or switch component is missing");
        }
    }
}
