﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

    public float Speed = 1f / 12f;
    public Vector3 Axis = Vector3.up;
    public bool IsRotating = true;
	
	void Update()
    {
        if (IsRotating)
            gameObject.transform.Rotate(Axis, Speed * Time.deltaTime);
	}
}
