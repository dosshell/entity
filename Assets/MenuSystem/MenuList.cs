﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuList : MonoBehaviour
{
    private List<MenuButton> menuButtons = new List<MenuButton>();
    public string MenuId;

    void Awake()
    {
        menuButtons.AddRange(GetComponentsInChildren<MenuButton>());
        menuButtons = menuButtons.FindAll(x => x.IsCurrent);
    }

    public void Show()
    {
        foreach (MenuButton btn in menuButtons)
            if (btn.IsCurrent)
                btn.gameObject.SetActive(true);
    }

    public void Hide()
    {
        foreach (MenuButton btn in menuButtons)
            btn.gameObject.SetActive(false);
    }

    public void OrderButtons(Vector2 Center, float buttonHeight, float buttonDistance = 0.05f)
    {
        float start = Center.y + (menuButtons.Count * (buttonHeight + buttonDistance) - buttonDistance) / 2f - buttonHeight/2f;
        float delta = buttonHeight + buttonDistance;

        for (int i = 0; i < menuButtons.Count; ++i)
        {
            Vector3 newPos = new Vector3(Center.x, start - delta * i, 0f);
            menuButtons[i].transform.position = newPos;
            var btn = menuButtons[i].GetComponent<GUIText>();
            btn.fontSize = (int) (Screen.height * buttonHeight);
        }
    }
}
