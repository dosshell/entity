﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuSystem : MonoBehaviour
{
    public MenuList StartMenu;
    protected Dictionary<string, MenuList> menuLists = new Dictionary<string, MenuList>();
    private Stack<MenuList> menuStack = new Stack<MenuList>();

    public Vector2 Center;
    public float ButtonHeightOfScreen = 0.1f;
    public float ButtonDistance = 0.1f;

    public int MenuDepth { get { return menuStack.Count; } }

    void Awake()
    {

    }

    void Start()
    {
        MenuList[] mls = GetComponentsInChildren<MenuList>();

        foreach (MenuList ml in mls)
            menuLists.Add(ml.MenuId, ml);

        foreach (KeyValuePair<string, MenuList> ml in menuLists)
        {
            ml.Value.Hide();
            ml.Value.OrderButtons(Center, ButtonHeightOfScreen, ButtonDistance);
        }
        PushMenuList(StartMenu);
    }

    virtual public void OnButtonPressed(string buttonId)
    {

    }

    public void PushMenuList(string menuListId)
    {
        PushMenuList(menuLists[menuListId]);
    }

    public void PushMenuList(MenuList ml)
    {
        if (menuStack.Count > 0)
            menuStack.Peek().Hide();

        menuStack.Push(ml);
        ml.Show();
    }

    public void PopMenuList()
    {
        MenuList ml = menuStack.Pop();
        ml.Hide();
        if (menuStack.Count > 0)
            menuStack.Peek().Show();
    }
}
