﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PlattformType { All, Mobile, Desktop }
public class MenuButton : MonoBehaviour
{
    public AudioClip DownSound;
    public AudioClip UpSound;
    public string ButtonId = "Not Set";
    public bool IsBack = false;
    public PlattformType OnlyInPlattform;

    private MenuSystem mother; //MenuList

    private List<RuntimePlatform> MobileList = new List<RuntimePlatform>() {
                                               RuntimePlatform.Android,
                                               RuntimePlatform.IPhonePlayer,
                                               //RuntimePlatform.WindowsWebPlayer,
                                               //RuntimePlatform.WP8Player,
                                               //RuntimePlatform.BlackBerryPlayer,
                                               //RuntimePlatform.OSXWebPlayer,
                                               RuntimePlatform.WebGLPlayer,
                                               RuntimePlatform.TizenPlayer,
                                               RuntimePlatform.WindowsPlayer
                                           };

    void Awake()
    {
        mother = GetComponentInParent<MenuSystem>();
        if (!IsCurrent)
            GetComponent<GUIText>().enabled = false;
    }

    public bool IsCurrent
    {
        get
        {
            switch (OnlyInPlattform)
            {
                case PlattformType.Mobile:
                    if (!MobileList.Contains(Application.platform))
                        return false;
                    break;

                case PlattformType.Desktop:
                    if (MobileList.Contains(Application.platform))
                        return false;
                    break;

                case PlattformType.All:
                    break;

                default:
                    break;
            }
            return true;
        }
    }

    void OnMouseDown()
    {
        if (DownSound != null)
            mother.gameObject.GetComponent<AudioSource>().PlayOneShot(DownSound);
    }

    void OnMouseUp()
    {
        if (UpSound != null)
            mother.gameObject.GetComponent<AudioSource>().PlayOneShot(UpSound);
        mother.OnButtonPressed(ButtonId);
    }

    void Update()
    {
        if (IsBack)
        {
#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR || UNITY_WEBGL
            if (Input.GetKeyDown(KeyCode.Escape))
                OnMouseDown();
            else if (Input.GetKeyUp(KeyCode.Escape))
                OnMouseUp();
#else
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OnMouseDown();
                OnMouseUp();
            }
#endif
        }
    }
}
