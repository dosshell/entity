﻿using UnityEngine;
using System.Collections;

public class MainMenu : MenuSystem
{
    public override void OnButtonPressed(string buttonId)
    {
        switch(buttonId.ToLower())
        {
            case "newgame":
                Application.LoadLevel("lobby");
                break;

            case "options":
                PushMenuList("options");
                break;

            case "exit":
                Invoke("Exit", 0.2f); //For sound effect
                break;

            case "back":
                PopMenuList();
                break;

            case "reset":
                var t = GameObject.FindGameObjectWithTag("LevelUnlocker");
                var k = t.GetComponent<LevelUnlocker>();
                k.LockAll();

                break;

            default:
                Debug.Log("Unknown button: " + buttonId);
                break;
        }
    }

    private void Exit()
    {
        Application.Quit();
    }
}
